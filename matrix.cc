#include "matrix.h"
#include <iostream>
#include <vector>
#include <stdlib.h>

using namespace std;

Matrix::Matrix(){
	rows_=0;
	cols_=0;
}
Matrix::Matrix(const Matrix& m){
	(*this).Resize(m.rows(),m.cols());
	for(int i=0;i<m.rows();++i){
		for(int j=0;j<m.cols();++j){
			(*this)(i,j)=m(i,j);
		}
	}
}
Matrix::Matrix(int rows, int cols){
	(*this).Resize(rows, cols);
}

int& Matrix::operator()(int r, int c){
	return values_[Sub2Ind(r, c)];
}
const int& Matrix::operator()(int r, int c) const{
	return values_[Sub2Ind(r, c)];
}

Matrix Matrix::operator+() const{
	return *this;
}
Matrix Matrix::operator-() const{
	Matrix temp = *this;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j) = -(*this)(i,j);
		}
	}
	return temp;
}

Matrix Matrix::Transpose() const{
	int row = (*this).cols(), col = (*this).rows();
	Matrix temp(row,col);
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j) = (*this)(j,i);
		}
	}
	return temp;
}

istream& operator>>(istream& is, Matrix& m){
	int row,col;
	is >> row >> col;
	m.Resize(row,col);
	for(int i=0;i<row;++i){
		for(int j=0;j<col;++j){
			int temp;
			cin >> temp;
			m(i,j) = temp;
		}
	}
	return is;
}

ostream& operator<<(ostream& os, const Matrix& m){
	int row = m.rows(), col = m.cols();
	for(int i=0;i<row;++i){
		for(int j=0;j<col;++j){
			os << m(i,j) << " ";
		}
		os << endl;
	}
	return os;
}

Matrix operator+(const Matrix& lm, const Matrix& rm){
	Matrix temp(lm);
	if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	else{
		for(int i=0;i<temp.rows();++i){
			for(int j=0;j<temp.cols();++j){
				temp(i,j)+=rm(i,j);
			}
		}	
	}
	return temp;
}

Matrix operator-(const Matrix& lm, const Matrix& rm){
	Matrix temp(lm);
	if(lm.rows()!=rm.rows() || lm.cols()!=rm.cols()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	else{
		for(int i=0;i<temp.rows();++i){
			for(int j=0;j<temp.cols();++j){
				temp(i,j)-=rm(i,j);
			}
		}	
	}
	return temp;
}

Matrix operator*(const Matrix& lm, const Matrix& rm){
	Matrix temp(lm.rows(),rm.cols());
	if(lm.cols()!=rm.rows()){
		cout << "Invalid operation" << endl;
		exit(0);
	}
	else{
		for(int i=0;i<temp.rows();++i){
			for(int j=0;j<temp.cols();++j){
				int value=0;
				for(int k=0;k<lm.cols();++k){
					value+=lm(i,k)*rm(k,j);
				}
				temp(i,j)=value;
			}
		}
	}
	return temp;
}

Matrix operator+(const int& a, const Matrix& rm){
	Matrix temp = rm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)=a+temp(i,j);
		}
	}
	return temp;
}
Matrix operator-(const int& a, const Matrix& rm){
	Matrix temp = rm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)=a-temp(i,j);
		}
	}
	return temp;
}
Matrix operator*(const int& a, const Matrix& rm){
	Matrix temp = rm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)=a*temp(i,j);
		}
	}
	return temp;
}
Matrix operator+(const Matrix& lm, const int& a){
	Matrix temp = lm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)+=a;
		}
	}
	return temp;
}
Matrix operator-(const Matrix& lm, const int& a){
	Matrix temp = lm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)-=a;
		}
	}
	return temp;
}
Matrix operator*(const Matrix& lm, const int& a){
	Matrix temp = lm;
	for(int i=0;i<temp.rows();++i){
		for(int j=0;j<temp.cols();++j){
			temp(i,j)*=a;
		}
	}
	return temp;
}