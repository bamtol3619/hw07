#include <iostream>
#include <vector>

#ifndef __hw07__draw_shape__
#define __hw07__draw_shape__

using namespace std;

class Canvas {
 public:
  Canvas(size_t row, size_t col);
  ~Canvas();

  // Canvas 크기를 w x h 로 변경한다. 그려진 내용은 보존한다.
  void Resize(size_t w, size_t h);

  // (x,y) 위치에 ch 문자를 그린다. 범위 밖의 x,y 는 무시한다.
  bool Draw(int x, int y, char brush);

  // 그려진 내용을 모두 지운다 ('.'으로 초기화)
  void Clear();
  
  int row() const{return row_;}
  int col() const{return col_;}

 private:
  // 그려진 모양을 저장할 수 있도록 데이터멤버를 정의 (resize 가능에 주의)
  size_t row_, col_;
  vector<string> map;
  friend ostream& operator<<(ostream& os, const Canvas& c);
};

class Shape {
 public:
  virtual ~Shape(){}
  virtual void Draw(Canvas* canvas) const = 0;
  virtual string type() const = 0;
  int x() const{return x_;}
  int y() const{return y_;}
  int w() const{return w_;}
  int h() const{return h_;}
  char brush() const{return brush_;}
 protected:
 	int x_,y_,w_,h_;
 	char brush_;
};

class Rectangle : public Shape {
	public:
		Rectangle(){}
		virtual ~Rectangle(){}
		virtual string type() const{return "rect";}
		virtual void Draw(Canvas* canvas) const;
	private:
		friend istream& operator>>(istream& is, Rectangle& r);
};

class UpTriangle : public Shape {
	public:
		UpTriangle(){}
		virtual ~UpTriangle(){}
		virtual string type() const{return "tri_up";}
		virtual void Draw(Canvas* canvas) const;
	private:
		friend istream& operator>>(istream& is, UpTriangle& t);
};

class DownTriangle : public Shape {
	public:
		DownTriangle(){}
		virtual ~DownTriangle(){}
		virtual string type() const{return "tri_down";}
		virtual void Draw(Canvas* canvas) const;
	private:
		friend istream& operator>>(istream& is, DownTriangle& d);
};

class Diamond : public Shape {
	public:
		Diamond(){}
		virtual ~Diamond(){}
		virtual string type() const{return "diamond";}
		virtual void Draw(Canvas* canvas) const;
	private:
		friend istream& operator>>(istream& is, Diamond& dm);
};

istream& operator>>(istream& is, Rectangle& r);
istream& operator>>(istream& is, UpTriangle& t);
istream& operator>>(istream& is, DownTriangle& d);
istream& operator>>(istream& is, Diamond& dm);

#endif