#include <iostream>
#include <vector>
#include <stdlib.h>
#include "draw_shape.h"

using namespace std;

Canvas::Canvas(size_t row, size_t col) : row_(row), col_(col){(*this).Resize(col,row);}
Canvas::~Canvas(){}

void Canvas::Resize(size_t w, size_t h){
	map.clear();
	string temp;
	row_=h;
	col_=w;
	for(int i=0;i<w;++i)	temp.push_back('.');
	for(int i=0;i<h;++i)	map.push_back(temp);
}

bool Canvas::Draw(int x, int y, char brush){
	if(x>=col_ || x<0 || y>=row_ || y<0)	return false;
	else{
		map[y][x]=brush;
		return true;
	}
}

void Canvas::Clear(){
	map.clear();
	string temp;
	for(int i=0;i<col_;++i)	temp.push_back('.');
	for(int i=0;i<row_;++i)	map.push_back(temp);
}

ostream& operator<<(ostream& os, const Canvas& c){
	cout << " ";
	for(int i=0;i<c.col_;++i)	cout << i%10;
	cout << endl;
	for(int i=0;i<c.row_;++i){
		cout << i%10 << c.map[i] << endl;
	}
	return os;
}

void Rectangle::Draw(Canvas* canvas) const{
	for(int i=0;i<canvas->row();++i){
		if(i>=this->y() && i<this->y()+this->h()){
			for(int j=0;j<canvas->col();++j){
				if(j>=this->x() && j<this->x()+this->w())	canvas->Draw(j,i,this->brush());
			}
		}
	}
}

void UpTriangle::Draw(Canvas* canvas) const{
	for(int i=0;i<canvas->row();++i){
		if(i>=this->y() && i<this->y()+this->h()){
			for(int j=0;j<canvas->col();++j){
				if(j>=this->x()-(i-this->y()) && j<=this->x()+(i-this->y()))	canvas->Draw(j,i,this->brush());
			}
		}
	}
}

void DownTriangle::Draw(Canvas* canvas) const{
	for(int i=0;i<canvas->row();++i){
		if(i<=this->y() && i>this->y()-this->h()){
			for(int j=0;j<canvas->col();++j){
				if(j>=this->x()-(this->y()-i) && j<=this->x()+(this->y()-i))	canvas->Draw(j,i,this->brush());
			}
		}
	}
}

void Diamond::Draw(Canvas* canvas) const{
	for(int i=0;i<canvas->row();++i){
		if(i>=this->y() && i<=this->y()+2*this->h()){
			for(int j=0;j<canvas->col();++j){
				if(j>=this->x()-(this->h()-abs(this->h()+this->y()-i)) && j<=this->x()+(this->h()-abs(this->h()+this->y()-i)))	canvas->Draw(j,i,this->brush());
			}
		}
	}
}

istream& operator>>(istream& is, Rectangle& r){
	int x,y,w,h;
	char brush;
	is >> x >> y >> w >> h >> brush;
	r.x_=x;
	r.y_=y;
	r.w_=w;
	r.h_=h;
	r.brush_=brush;
	return is;
}

istream& operator>>(istream& is, UpTriangle& t){
	int x,y,h;
	char brush;
	is >> x >> y >> h >> brush;
	t.x_=x;
	t.y_=y;
	t.h_=h;
	t.brush_=brush;
	return is;
}

istream& operator>>(istream& is, DownTriangle& d){
	int x,y,h;
	char brush;
	is >> x >> y >> h >> brush;
	d.x_=x;
	d.y_=y;
	d.h_=h;
	d.brush_=brush;
	return is;
}

istream& operator>>(istream& is, Diamond& dm){
	int x,y,h;
	char brush;
	is >> x >> y >> h >> brush;
	dm.x_=x;
	dm.y_=y;
	dm.h_=h;
	dm.brush_=brush;
	return is;
}